From this performance experiment, I tested the program's performance
using a TOTAL FILE SIZE to write out as 100Mb. I then used various 
BUFFER SIZEs from 1byte-100Mb to test the write speed performance.
Starting at a buffer size of 1byte, the average write speed is 
504 KB/sec. I then doubled the buffer size each time to see how it
affected the write speed.

The write speed was slowest when the buffer size was the smallest.
I incremented it by a factor of 2 for each new test. The write speed
got faster and faster as I increased the buffer size. Only up until a
certain point. The sweet spot was when the buffer size was around 50Mb,
half of the file size. And when the buffer size was 100mb, the speed
dropped. 

I think this happens because there needs to be some balance when 
choosing a buffer size. A size too small will of course not take up
much memory space, but it will also take longer to complete the task.
A size too big and that buffer takes up too much memory space, causing
the program to run slower than expected. When choosing a buffer size,
try to find the sweet spot between the smallest and biggest buffer size.
